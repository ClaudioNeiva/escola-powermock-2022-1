package br.ucsal.bes20221.testequalidade.escola.domain;

public enum SituacaoAluno {
	ATIVO, CANCELADO;
}
