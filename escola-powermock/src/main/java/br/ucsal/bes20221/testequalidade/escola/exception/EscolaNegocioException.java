package br.ucsal.bes20221.testequalidade.escola.exception;

public class EscolaNegocioException extends Exception {

	private static final long serialVersionUID = 1L;

	public EscolaNegocioException(String message) {
		super(message);
	}
	
}
