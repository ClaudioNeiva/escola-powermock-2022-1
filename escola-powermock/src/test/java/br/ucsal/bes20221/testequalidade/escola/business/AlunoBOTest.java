package br.ucsal.bes20221.testequalidade.escola.business;

import java.sql.SQLException;

public class AlunoBOTest {

	/**
	 * Verificar o calculo da idade.
	 * 
	 * Considerando que o ano atual é 2020, um aluno nascido em 2003 tera 17 anos.
	 * 
	 * Caso de teste
	 * 
	 * # | entrada | saida esperada
	 * 
	 * 1 | ano atual=2020 ; ano de nascimento=2003 | idade=17
	 * 
	 * @throws SQLException
	 * 
	 */
	public void testarCalculoIdadeAluno1() throws SQLException {
	}

}
